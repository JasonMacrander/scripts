#!/usr/bin/env python

from Bio import SeqIO
from subprocess import call
import sys
import glob
import re


Usage = """

FUNCTION:
	This program is to be used in combination with EdgeR cluster analyses to retrieve clustered FASTA files. 

USAGE:
	EdgeR_clusters_Extract.py N PATH

	EdgeR_clusters_Extract.py [number of clusters] [path to Trinity assemlby file]

OUTPUT:
	Multiple .fasta files for each clsuter.

Thanks to Bill Browne for allowing me to stealing this "USAGE" information.

"""
def subcluster(numbers, fasta_file):

	#fasta_file = "/data2/dalylab/z3_Trinity_Assembly/M_griffithsi_E/M_griffithsi_Combined/M_griffithsi_Trinity.fasta" # Input fasta file
	wanted_file = "subcluster_%d_log2_medianCentered_fpkm.matrix" % numbers # Input interesting sequence IDs, one per line
	result_file = "subcluster_%d_out.fasta" % numbers # Output fasta file

	wanted = set()
	with open(wanted_file) as f:
	    for line in f:
	        line = line.strip()
	        line = line.split("\t")
	        line = line [0]
	        #print line
	        if line != "":
	            wanted.add(line)

	fasta_sequences = SeqIO.parse(open(fasta_file),'fasta')
	with open(result_file, "w") as f:
	    for seq in fasta_sequences:
	    	#print seq.id
	        if seq.id in wanted:
	            SeqIO.write([seq], f, "fasta")


def transdecoder(numbers):
	""" Calls the program transdecoder to find """
	#call (["nohup" , "TransDeoder.LongOrfs -m 50 -t %s >>./log/%s_log 2>>./log/%s_err &"]) % result_file, numbers, numbers
	command = "-m 50 -t subcluster_%d_out.fasta" % numbers
	call (["TransDecoder.LongOrfs" , command])

call(["mkdir", "log"])

if len(sys.argv) != 3:
	# if invalid arguments are called, print usage in the terminal
	print Usage

else:
	File = sys.argv[2]

	subclusters = int(sys.argv[1])
	 
	List = range(1, (subclusters + 1))
	for i in List:
		if i <= subclusters:
			subcluster(i, File)
			transdecoder(i)
		else:
			print "complete"#
