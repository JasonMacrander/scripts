#DALY LAB SCRIPTS
This is a repository for scripts (python, perl, R, whatever!) that are useful and may be useful to others. 

Use git push/pull to submit/pull scripts accordingly.

You can modify your .bash_profile to link these scripts directly to your working enviornment.

You may have to modify them to make them executable on your account:

    chmod u+x *

or 

    chmod u+x [script name]